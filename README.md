# Savannah Life Simulator #

## Sol_vegetarian_drink ##
Az állatnak legyen tárolva, hogy mennyire szomjas. Ha elég szomjas,
és sekély vízben áll, nézze meg, hogy van-e ragadozó az adott cellán,
ha nincs igyon.

## Sol_predator_reproduce ##
Az állatnak legyen tárolva, hogy milyen faj, a kora, a neme, hogy mennyire
éhes ill.szomjas. Ha felnõtt, nem éhes és nem szomjas, nézzen szét a celláján,
hogy van-e rajta vele azonos fajú, ellentétes nemû felnõtt példány, aki
nem éhes és nem szomjas, ha talál ilyet, szaporodjanak, ezáltal létrejön
egy új példány az adott fajból újszülöttként.

## Sol_animalnutrition_graze ##
A növényevõ állat azon a cellán legeljen, amelyiken épp tartózkodik.
A növényt nulla nagyságig fogyaszthatja, kivéve ha közben jóllakik.
Csak akkor legelhet, ha van az adott cellán növény.

## Sol_animalnutrition_hunt ##
A ragadozó azon a cellán keres zsákmányt, amin épp tartózkodik.
Ki kell választania a leggyengébbet (a növényevõk közül) és meg 
kell próbálnia levadászni. Ha sikerül jóllakik és a celláról eltûnik
az elfogyasztott zsákmányállat.

## Sol_vegetarian_move ##
A növényevõ mozogjon arra a cellára, ahol a legtöbbet tud legelni, akár
több lépésbõl is. Oda ne lépjen, ahol van ragadozó.

## Sol_predator_move ##
A ragadozó állat oda lépjen -akár több lépésbõl- , ahol a legnagyobb
valószínûséggel ejt el egy prédát.

## Sol_savannah_run ##
Indítsa el a szavannán az életet egy megadott pontból. Ha elfogynak 
az állatok, azaz kihal a szavanna, akkor megáll.

## Sol_savannah_init ##
Inicializálja a szavannát a megadott paraméterek alapján,
felkészíti az "indításhoz".

## Sol_savannah_passoneday ##
Futtasson végig a szavanna életén egy napot, ezzel megváltoztatja a 
szavanna állapotát.

## Sol_data_getcountofanimals ##
Adja vissza a Data-ban az állatok számainak összegét.

## Sol_data_getmaxofanimals ##
Gyártson egy példányt abból az állatból, amibõl a legtöbbet lehet csinálni

## Sol_presentationwpf_cellcontrol_refresh ##
Egy cella kijelzését frissíse a UI-en.

## Sol_presentationwpf_savannahwindow ##
Meg kell jelenítenie a cellákon lévõ állatokat, illetve az állatokat, a
kölyköknek, ill. kifejlett példányoknak való helyre kell illesztenie