﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Savannah.Core;

namespace Savannah.Presentation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBoxX_TextChanged(object sender, EventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBoxX.Text, out parsedValue))
            {
                textBoxX.Text = "";
            }
        }

        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBoxY.Text, out parsedValue))
            {
                textBoxY.Text = "";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
          
        }
    }
}
