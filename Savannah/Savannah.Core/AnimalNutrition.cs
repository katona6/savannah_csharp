﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    static public class PredatorHunt
    {
        /// <summary>
        /// Kiválasztja a leggyengébb zsákmányt arról a celláról, amin van, utána megpróbálja levadászni.
        /// </summary>
        /// <param name="pPred">Ragadozó</param>
        /// <param name="pCell">Zsákmány</param>
        /// <returns>Sikerült-e elkapnia</returns>
        static public bool Hunt(this Predator pPred, Cell pCell)
        {

            int min = int.MaxValue;

            AnimalBase Prey = null;

            foreach (AnimalBase item in pCell.GetAnimalList())
            {
                if (item is Vegetarian)
                {
                    if (item.SurvivalRate < min)
                    {
                        Prey = item;
                    }
                }
            }

            if (Prey != null)
            {
                if (pPred.TryHunt(Prey) == true)
                {
                    pPred.HungerLevel += 10;
                    pCell.RemoveAnimal(Prey);
                    return true;
                }
                else return false;
            }
            else return false;
        }

        static Random r = new Random();

        /// <summary>
        /// Ez a függvény eldönti, hogy a ragadozó elkapja-e a zsákmányt és visszatér egy logikai értékkel.
        /// </summary>
        /// <param name="pred">Ragadozó</param>
        /// <param name="prey">Zsákmány</param>
        /// <returns></returns>
        static public bool TryHunt(this Predator pPred, AnimalBase pPrey)
        {
            if (pPred.AttackPower >= pPrey.SurvivalRate)
                return true;
            int point = r.Next(0, pPrey.SurvivalRate + 1);
            if (point <= pPred.AttackPower)
            {
                return true;
            }
            else return false;
        }
    }

    /// <summary>
    /// Osztály a növényevők legeléséhez.
    /// </summary>
    public static class VegetarianGraze
    {
        /// <summary>
        /// Függvény a legelésre. Ha az állat tudott legelni, akk return true;
        /// </summary>
        /// <param name="pAnimal">Növényevő állat</param>
        /// <param name="pCell">Az a cella amin tartózkodik</param>
        /// <returns>Sikerült-e legelnie vagy sem.</returns>
        public static bool Graze(this Vegetarian pAnimal,ref Cell pCell)
        {
            if (pCell.Plantp == null || pCell.Plantp.PlantSize <= 0) return false;

            while (pAnimal.HungerLevel < 13  &&  pCell.Plantp.PlantSize > 0)
            {
                --pCell.Plantp.PlantSize;
                ++pAnimal.HungerLevel;
            }

            return true;
        }
    }
}
