﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// Gazellák osztálya
    /// A gazellák közös tulajdonságait, viselkedéseit tartalmazza
    /// </summary>
    public class Gazelle :Vegetarian
    {
        internal override int RangeOfView
        {
            get { return 5; }
        }

        internal override int RangeOfMove
        {
            get { return 3; }
        }

        protected override void Eat()
        {
            throw new NotImplementedException();
        }


        public Gazelle()
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            SurvivalRate = 90 + new Random().Next(-8, 9);
            targetCell = null;
        }

        public Gazelle(int px, int py)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            SurvivalRate = 90 + new Random().Next(-8, 9);
            targetCell = null;
        }

        public Gazelle(int px, int py, Sex ps)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            SurvivalRate = 90 + new Random().Next(-8, 9);
            SexEnumProperty = ps;
            targetCell = null;
        }


        internal override void Drink(ref Cell thisCell)
        {
            if (thisCell.CellType == FieldType.water && /*thisCell.CellType == WaterDepth.shallow &&*/ this.ThirstyLevel > 4)
            {
                List<AnimalBase> animals = new List<AnimalBase>(thisCell.GetAnimalList());
                int i = 0;
                while (i < animals.Count && !(animals[i] is Predator)) { i++; }
                if (i == animals.Count)
                {
                    this.ThirstyLevel = 0;
                }
            }
        }


        internal void MoveB(ref Cell fromcell, ref Cell tocell)
        {
            if (tocell.AddAnimal(this))
            {
                fromcell.RemoveAnimal(this);
            }
        }
    }
}
