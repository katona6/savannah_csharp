﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    class DayPassedEventArgs
    {
        private readonly Cell[,] args;
        internal Cell[,] Args
        {
            get
            {
                return args;
            }
        }

        public DayPassedEventArgs(Cell[,] pArgs)
        {
            args = pArgs;
        }
    }
}
