﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// A növényzet tulajdonságait tartalmazó osztály
    /// </summary>
    public class Plant
    {
        public Plant()
        {

        }

        public Plant(int psize)
        {
            this.PlantSize = psize;
        }


        /// <summary>
        /// Property a növény méretéhez
        /// </summary>
        public int PlantSize { get; set; }

        /// <summary>
        /// Property a növény max méretéhez
        /// </summary>
        public int MaxPlantSize { get; set; }

        /// <summary>
        /// A növény egy egységnyi növekedése, ha nem érte még el a maximális méretét
        /// </summary>
        public void Grow()
        {
            PlantSize += 4;
        }

        /// <summary>
        /// Egy állat eszik a növényből
        /// </summary>
        /// <param name="animalHungerLevel">Ennyi egységet eszik meg az állat a növényből</param>
        /// <returns>Visszadja mennyire éhes még az állat</returns>
        public int TryToEat(int animalHungerLevel)
        {
            PlantSize = PlantSize - animalHungerLevel;
            if (PlantSize < 0)
            {
                int animalHungerLevelLeft = PlantSize * -1;
                PlantSize = 0;
                return animalHungerLevelLeft;
            }
            else
                return 0;
        }
    }
}