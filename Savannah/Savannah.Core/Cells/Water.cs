﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core.Cells
{
    /// <summary>
    /// Vízi cellák osztálya.  
    /// </summary>
    class Water : CellBase
    {
        /// <summary>
        /// A cellában tartózkodó állatok listája
        /// </summary>
        private List<AnimalBase> Animals;

        /// <summary>
        /// A mezőre lépő állatot hozzáadja az állatok listájához
        /// </summary>
        /// <param name="p_Animal">Az az állat, amely a mezőre lépne</param>
        public void AddAnimal(AnimalBase p_Animal)
        {
            if (p_Animal.AnimalTypeEnumProperty == AnimalType.aquatic)
            {
                if (Animals.Count <= 3)
                {
                    Animals.Add(p_Animal);
                }
                else throw new Exception("3-nál több állat nem tartózkodhat a cellában(kiv. születés)!");
            }
            else throw new Exception("Vízbe csak vízi állatok kerülhetnek!");
        }

        /// <summary>
        /// Ha születik egy állat, ezzel lehet hozzáadni a cellához
        /// </summary>
        /// <param name="p_BabyAnimal">Az újszülött állat</param>
        public void AnimalBirth(AnimalBase p_BabyAnimal)
        {
            Animals.Add(p_BabyAnimal);
        }

        /// <summary>
        /// A cella növényzete
        /// </summary>
        private readonly Plant Terrain;

        /// <summary>
        /// A víz mélysége
        /// </summary>
        private readonly WaterDepth Depth;

        /// <summary>
        /// Konstruktor állatok listája nélkül, csak növényzettel
        /// </summary>
        /// <param name="p_Plant">A növényzet fajtája</param>
        public Water(Plant p_Plant, WaterDepth p_Depth)
        {
            this.Terrain = p_Plant;
            this.Depth = p_Depth;
        }

        /// <summary>
        /// Konstruktor növényzettel és állatok listájával
        /// </summary>
        /// <param name="p_Plant">Növényzet fajtája</param>
        /// <param name="p_Animals">Állatok listája</param>
        public Water(Plant p_Plant, WaterDepth p_Depth, List<AnimalBase> p_Animals)
        {
            foreach (AnimalBase item in p_Animals)
            {
                if (item.AnimalTypeEnumProperty == AnimalType.terrestial)
                {
                    throw new Exception("Vízbe csak vízi állatok kerülhetnek!");
                }
            }
            this.Terrain = p_Plant;
            this.Depth = p_Depth;
            this.Animals = p_Animals;
        }
    }
}
