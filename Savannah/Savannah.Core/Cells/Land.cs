﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core.Cells
{
    /// <summary>
    /// Szárazföldi cellák osztálya.  
    /// </summary>
    class Land : CellBase
    {
        /// <summary>
        /// A cellában tartózkodó állatok listája
        /// </summary>
        private List<AnimalBase> Animals;

        /// <summary>
        /// A mezőre lépő állatot hozzáadja az állatok listájához
        /// </summary>
        /// <param name="p_Animal">Az az állat, amely a mezőre lépne</param>
        public void AddAnimal(AnimalBase p_Animal)
        {
            if (p_Animal.AnimalTypeEnumProperty == AnimalType.terrestial)
            {
                if (Animals.Count <= 3)
                {
                    Animals.Add(p_Animal);
                }
                else throw new Exception("3-nál több állat nem tartózkodhat a cellában(kiv. születés)!");
            }
            else throw new Exception("Szárazföldre csak szárazföldi állatok kerülhetnek!");      
        }

        /// <summary>
        /// Ha születik egy állat, ezzel lehet hozzáadni a cellához
        /// </summary>
        /// <param name="p_BabyAnimal">Az újszülött állat</param>
        public void AnimalBirth(AnimalBase p_BabyAnimal)
        {
            Animals.Add(p_BabyAnimal);
        }

        /// <summary>
        /// A cella növényzete
        /// </summary>
        private Plant Terrain;

        /// <summary>
        /// Konstruktor állatok listája nélkül, csak növényzettel
        /// </summary>
        /// <param name="p_Plant">A növényzet fajtája</param>
        public Land(Plant p_Plant)
        {
            this.Terrain = p_Plant;
        }

        /// <summary>
        /// Konstruktor növényzettel és állatok listájával
        /// </summary>
        /// <param name="p_Plant">Növényzet fajtája</param>
        /// <param name="p_Animals">Állatok listája</param>
        public Land(Plant p_Plant, List<AnimalBase> p_Animals)
        {
            foreach (AnimalBase item in p_Animals)
            {
                if (item.AnimalTypeEnumProperty == AnimalType.aquatic)
                {
                    throw new Exception("Szárazföldre csak szárazföldi állatok kerülhetnek!");
                }
            }
            this.Terrain = p_Plant;
            this.Animals = p_Animals;
        }
    }
}
