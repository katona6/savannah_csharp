﻿namespace Savannah.Core
{
    /// <summary>
    /// Felsorolásos típus a nemekre
    /// </summary>
    public enum Sex { male, female }

    /// <summary>
    /// Felsorolásos típus az állatok fajtájára (sz.földi / vízi)
    /// </summary>
    public enum AnimalType { terrestial, aquatic }

    /// <summary>
    /// Felsorolásos típus a növények fajtájára
    /// </summary>
    public enum PlantType { grass, tree }

    /// <summary>
    /// Felsorolásos típus a mezők típusára (sz.föld / víz)
    /// </summary>
    public enum FieldType { land, water }

    /// <summary>
    /// Felsorolásos típus a víz mélységére
    /// </summary>
    public enum WaterDepth { shallow, deep }
}
