﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// A hiénák osztálya.
    /// A hiénákra jellemző tulajdonságokat és viselkedésformákat tartalmazza.
    /// </summary>
    public class Hyena : Predator
    {
        internal override int RangeOfMove
        {
            get
            {
                return 3;
            }
        }

        internal override int RangeOfView
        {
            get
            {
                return 6;
            }
        }


        public Hyena()
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 70 + new Random().Next(-5, 6);
            targetCell = null;
        }

        public Hyena(int px, int py)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 70 + new Random().Next(-5, 6);
            targetCell = null;
        }

        public Hyena(int px, int py, Sex ps)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 70 + new Random().Next(-5, 6);
            SexEnumProperty = ps;
            targetCell = null;
        }



        internal override void Drink(ref Cell thisCell)
        {
            throw new NotImplementedException();
        }

        protected override void Eat()
        {
            throw new NotImplementedException();
        }
    }
}
