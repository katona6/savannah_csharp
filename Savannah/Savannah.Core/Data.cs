﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// Data osztály a megadott adatoknak, mennyi állat, mekkora a szavanna stb.
    /// </summary>
    public class Data
    {
        internal int SavannahSizeX { get; set; } /*A szavanna mérete X tengely szerint*/
        internal int SavannahSizeY { get; set; } /*A szavanna mérete Y tengely szerint*/
        internal int CrocodileNumber { get; set; } /*A krokodilok száma a szavannán*/
        internal int GazelleNumber { get; set; } /*A gazellák száma a szavannán*/
        internal int GiraffeNumber { get; set; } /*A zsiráfok száma a szavannán*/
        internal int HyenaNumber { get; set; } /*A hiénák száma a szavannán*/
        internal int LionNumber { get; set; } /*Az oroszlánok száma a szavannán*/
        internal int PlantNumber { get; set; } /*A növények száma a szavannán*/

        public Data()
        {
            SavannahSizeX = 0;
            SavannahSizeY = 0;
            CrocodileNumber = 0;
            GazelleNumber = 0;
            GiraffeNumber = 0;
            HyenaNumber = 0;
            LionNumber = 0;
            PlantNumber = 0;
        }

        /// <summary>
        /// Visszaadja a data-példányban lévő állatok számát
        /// </summary>
        /// <returns></returns>
        internal int GetCountOfAnimals()
        {
            return CrocodileNumber + GazelleNumber + GiraffeNumber + HyenaNumber + LionNumber;
        }  
        
        internal AnimalBase GetMaxOfAnimal()
        {
            int max = GazelleNumber;
            AnimalBase animal = new Gazelle();

            if (CrocodileNumber>max)
            {
                max = CrocodileNumber;
                animal = new Crocodile();
            }
            if (GiraffeNumber>max)
            {
                max = GiraffeNumber;
                animal = new Giraffe();
            }
            if (HyenaNumber>max)
            {
                max = HyenaNumber;
                animal = new Hyena();
            }
            if (LionNumber>max)
            {
                max = LionNumber;
                animal = new Lion();
            }

            return animal;
        }     
    }
}