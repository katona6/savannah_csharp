﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// Zsiráfok osztálya
    /// A zsiráfok közös tulajdonságait, viselkedéseit tartalmazza
    /// </summary>
    class Giraffe : Vegetarian
    {
        internal override int RangeOfMove
        {
            get
            {
                return 5;
            }
        }

        internal override int RangeOfView
        {
            get
            {
                return 8;
            }
        }


        public Giraffe()
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            SurvivalRate = 100 + new Random().Next(-10, 11);
            targetCell = null;
        }

        public Giraffe(int px, int py)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            SurvivalRate = 100 + new Random().Next(-10, 11);
            targetCell = null;
        }

        public Giraffe(int px, int py, Sex ps)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            SurvivalRate = 100 + new Random().Next(-10, 11);
            this.SexEnumProperty = ps;
            targetCell = null;
        }


        internal override void Drink(ref Cell thisCell)
        {
            throw new NotImplementedException();
        }

        protected override void Eat()
        {
            throw new NotImplementedException();
        }
    }
}
