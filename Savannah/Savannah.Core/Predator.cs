﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// A Ragadozók ősosztálya.
    /// Tartalmazza a Ragadozók KöZöS tulajdonságait és viselkedésformáit.
    /// </summary>
    public abstract class Predator : AnimalBase
    {
        internal override void Live(ref Cell[,] pMap)
        {
            Cell tempCell = null;

            foreach (Cell item in pMap)
            {
                if (item.CoordX == xCoordinate && item.CoordY == yCoordinate)
                {
                    tempCell = item;

                    break;
                }
            }

            if (HungerLevel < 3 && tempCell != null)
            {
                if (this.Hunt(tempCell))
                {
                    
                    CurrentAge++;
                    HungerLevel--;

                    return;
                }

            }

            if (canReproduce(tempCell))
            {
                if (Reproduce(ref tempCell))
                {
                    CurrentAge++;
                    HungerLevel--;

                    return;
                }
            }

            Move(ref pMap);


            CurrentAge++;
            HungerLevel--;
        }

        /// <summary>
        /// Move metódus implementációja növényevőknek
        /// </summary>
        /// <param name="possibleCells">A cellák, amiket az állat lát</param>
        internal override void Move(ref Cell[,] possibleCells)
        {
            Cell toCell = null;

            if (targetCell != null)
            {
                toCell = targetCell;
                DoTheMove(ref toCell, ref possibleCells);
                targetCell = null;

                return;
            }
            else
            {
                toCell = HungerLevel > 3 ? ChooseCellForReproduce(possibleCells) : ChooseCell(ref possibleCells);
            }


            DoTheMove(ref toCell, ref possibleCells);
        }

        /// <summary>
        /// ChooseCell fv. implementációja növényevőknek
        /// </summary>
        /// <param name="possibleCells">Ezekből a cellákból válogat</param>
        /// <returns>A cella amire majd megpróbál lépni</returns>
        protected override Cell ChooseCell(ref Cell[,] possibleCells)
        {
            Cell maxcell = null;

            foreach (Cell currentCell in possibleCells)
            {
                if (currentCell.CoordX == xCoordinate && currentCell.CoordY == yCoordinate)
                {
                    maxcell = currentCell;
                    break;
                }
            }

            int maxp = int.MinValue;
            int temp;
            foreach (Cell actCell in possibleCells)
            {
                temp = CalculatePoints(actCell);
                if (temp > maxp)
                {
                    maxp = temp;
                    maxcell = actCell;
                }
            }

            return maxcell;
        }

        /// <summary>
        /// A Calculate fv. implemetációja növényevőknek
        /// </summary>
        /// <param name="pCell"></param>
        /// <returns></returns>
        protected override int CalculatePoints(Cell pCell)
        {
            if (pCell.GetAnimalList().Where(x=>x is Vegetarian).Count() == 0)
            {
                return GetDistance(pCell, new Cell(xCoordinate, yCoordinate));
            }

            if (pCell.GetAnimalList().Count > 5 || pCell.GetAdultNumber() > 2)
            {
                return -1;
            }

            int min = int.MaxValue;
            foreach (var item in pCell.GetAnimalList())
            {
                if (item is Vegetarian && item.SurvivalRate<min)
                {
                    min = item.SurvivalRate;
                }
            }

            try
            {
                return (300 - min) * HungerLevel / GetDistance(pCell, new Cell(xCoordinate, yCoordinate));
            }
            catch(DivideByZeroException)
            {
                return 0;
            }
        }

        /// <summary>
        /// A DoTheMove metódus implementációja a vegetáriánusoknak.
        /// </summary>
        protected override void DoTheMove(ref Cell pTo, ref Cell[,] pMap)
        {
            if (pTo == null)
            {
                return;
            }


            if (xCoordinate - RangeOfMove > pTo.CoordX && yCoordinate - RangeOfMove > pTo.CoordX)
            {
                Savannah.savannah[xCoordinate - RangeOfMove, yCoordinate - RangeOfMove].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                xCoordinate -= RangeOfMove;
                yCoordinate -= RangeOfMove;

                targetCell = pTo;
                return;
            }

            if (xCoordinate + RangeOfMove < pTo.CoordX && yCoordinate + RangeOfMove < pTo.CoordY)
            {
                Savannah.savannah[xCoordinate + RangeOfMove, yCoordinate + RangeOfMove].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                xCoordinate += RangeOfMove-1;
                xCoordinate += RangeOfMove-1;

                targetCell = pTo;
                return;
            }

            if (yCoordinate - RangeOfMove > pTo.CoordY && xCoordinate + RangeOfMove < pTo.CoordX)
            {
                Savannah.savannah[xCoordinate + RangeOfMove, yCoordinate - RangeOfMove].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                yCoordinate -= RangeOfMove;
                xCoordinate += RangeOfMove;

                targetCell = pTo;
                return;
            }

            if (yCoordinate + RangeOfMove < pTo.CoordY && xCoordinate - RangeOfMove > pTo.CoordX)
            {
                Savannah.savannah[xCoordinate - RangeOfMove, yCoordinate + RangeOfMove].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                yCoordinate += RangeOfMove-1;
                xCoordinate -= RangeOfMove;

                targetCell = pTo;
                return;
            }

            if (xCoordinate - RangeOfMove > pTo.CoordX)
            {
                Savannah.savannah[xCoordinate - RangeOfMove, yCoordinate].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                xCoordinate -= RangeOfMove;
                yCoordinate = pTo.CoordY;

                targetCell = pTo;
                return;
            }

            if (xCoordinate + RangeOfMove < pTo.CoordX)
            {
                Savannah.savannah[xCoordinate + RangeOfMove, yCoordinate].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                xCoordinate += RangeOfMove-1;
                yCoordinate = pTo.CoordY;

                targetCell = pTo;
                return;
            }

            if (yCoordinate - RangeOfMove > pTo.CoordY)
            {
                Savannah.savannah[xCoordinate, yCoordinate - RangeOfMove].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                yCoordinate -= RangeOfMove;
                xCoordinate = pTo.CoordX;

                targetCell = pTo;
                return;
            }

            if (yCoordinate + RangeOfMove < pTo.CoordY)
            {
                Savannah.savannah[xCoordinate, yCoordinate + RangeOfMove].AddAnimal(this);
                Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
                yCoordinate += RangeOfMove-1;
                xCoordinate = pTo.CoordX;

                targetCell = pTo;
                return;
            }


            pTo.AddAnimal(this);
            Savannah.savannah[xCoordinate, yCoordinate].RemoveAnimal(this);
            xCoordinate = pTo.CoordX;
            yCoordinate = pTo.CoordY;
            targetCell = null;
        }

        /// <summary>
        /// Megadja a két cella távolságát(ha x-táv nagyobb akkor azt, ellenkező esetben y-távot)
        /// </summary>
        protected int GetDistance(Cell Cell1, Cell Cell2)
        {
            int dX = Math.Abs(Cell1.CoordX - Cell2.CoordX);
            int dY = Math.Abs(Cell1.CoordY - Cell2.CoordY);

            return dX > dY ? dX : dY;
        }

        /// <summary>
        /// Változó a ragadozó zsámányszerzési "pontjainak".
        /// </summary>
        private readonly int _AttackPower;

        /// <summary>
        /// Property a ragadozó zsákmányszerzési "pontjainak" elérésére.
        /// </summary>       
        public int AttackPower
        {
            get; set;
        }
    }
}
