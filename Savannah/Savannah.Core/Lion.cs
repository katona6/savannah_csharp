﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// Az Oroszlánok osztálya.
    /// Az Oroszlánokra jellemző tulajdonságokat és viselkedésformákat tartalmazza.
    /// </summary>
    public class Lion : Predator
    {
        internal override int RangeOfView
        {
            get
            {
                return 5;
            }
        }

        internal override int RangeOfMove
        {
            get
            {
                return 3;
            }
        }


        public Lion()
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 80 + new Random().Next(-5, 6);
            targetCell = null;
        }

        public Lion(int px, int py)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 80 + new Random().Next(-5, 6);
            targetCell = null;
        }

        public Lion(int px, int py, Sex ps)
        {
            AnimalTypeEnumProperty = AnimalType.terrestial;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 80 + new Random().Next(-5, 6);
            SexEnumProperty = ps;
            targetCell = null;
        }


        protected override void Eat()
        {
            throw new NotImplementedException();
        }

        internal override void Drink(ref Cell thisCell)
        {
            throw new NotImplementedException();
        }

        internal void MoveB(ref Cell fromCell, ref Cell toCell)
        {
            if (toCell.AddAnimal(this))
            {
                fromCell.RemoveAnimal(this);
            }
        }
    }
}
