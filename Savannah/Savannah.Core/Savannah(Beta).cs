﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// Szavanna osztály a szavanna működésére(erősen beta verzió)
    /// </summary>
    static class Savannah_Beta
    {
        /// <summary>
        /// Szavanna vízszintes hossza
        /// </summary>
        static int savannahXLength;
        /// <summary>
        /// Szavanna függőleges hossza
        /// </summary>
        static int savannahYLength;

        /// <summary>
        /// Cellák a szavannában(pl.:ezen vannak az állatok)
        /// </summary>
        static Cell[,] savannah;

        static Random rnd = new Random();

        /// <summary>
        /// Inicializálja a szavannát, a megadott paraméterek alapján
        /// </summary>
        /// <param name="parameters">Data osztály egy példánya, amiben benne van minden szükséges paraméter</param>
        static void init(Data parameters)
        {
            savannahXLength = parameters.SavannahSizeX;
            savannahYLength = parameters.SavannahSizeY;

            throw new NotImplementedException();
        }

        /// <summary>
        /// Elindítja az életet a szavannán, nem áll le amíg van rajta állat
        /// </summary>
        static internal void Run()
        {
            while(GetAllAnimal().Count > 0)
            {
                PassOneDay();
            }
        }

        /// <summary>
        /// Lefuttat egy "kört" a szavannán
        /// </summary>
        static internal void PassOneDay()
        {
            List<AnimalBase> animals = GetAllAnimal();
            int index;

            while(animals.Count>0)
            {
                index = rnd.Next(0, animals.Count);
                Cell[,] tempMap = CollectCells(animals[index]);
                animals[index].Live(ref tempMap);
                animals.RemoveAt(index);
            }
        }

        /// <summary>
        /// Kiválogatja a szavanna cellái közül, hogy az állat melyik cellákra léphet(figyelembe veszi hogy vízi v szárazföldi a cella)
        /// </summary>
        /// <param name="pAnimal">Ennek az állat körüli cellákból válogat(x és y koordinátája kell hogy legyen)</param>
        /// <returns></returns>
        static Cell[,] CollectCells(AnimalBase pAnimal)
        {
            int startX, startY, endX, endY;

            if (pAnimal.HungerLevel <= 2 || pAnimal.ThirstyLevel <= 3)
            {
                startX = pAnimal.xCoordinate - pAnimal.rangeofMove < 0 ? 0 : pAnimal.xCoordinate - pAnimal.rangeofMove;
                startY = pAnimal.yCoordinate - pAnimal.rangeofMove < 0 ? 0 : pAnimal.yCoordinate - pAnimal.rangeofMove;
                endX = pAnimal.xCoordinate + pAnimal.rangeofMove > savannahXLength ? savannahXLength : pAnimal.xCoordinate + pAnimal.rangeofMove;
                endY = pAnimal.yCoordinate + pAnimal.rangeofMove > savannahYLength ? savannahYLength : pAnimal.yCoordinate + pAnimal.rangeofMove;
            }
            else
            {
                startX = pAnimal.xCoordinate - pAnimal.rangeofView < 0 ? 0 : pAnimal.xCoordinate - pAnimal.rangeofView;
                startY = pAnimal.yCoordinate - pAnimal.rangeofView < 0 ? 0 : pAnimal.yCoordinate - pAnimal.rangeofView;
                endX = pAnimal.xCoordinate + pAnimal.rangeofView > savannahXLength ? savannahXLength : pAnimal.xCoordinate + pAnimal.rangeofView;
                endY = pAnimal.yCoordinate + pAnimal.rangeofView > savannahYLength ? savannahYLength : pAnimal.yCoordinate + pAnimal.rangeofView;
            }

            Cell[,] Collected = new Cell[Math.Abs(endX - startX), Math.Abs(endY - startY)];

            int lineindex = 0, columnindex = 0;
            for (int i = startX; i < endX; ++i)
            {
                for (int j = startY; j < endY; ++j)
                {
                    if ((int)pAnimal.AnimalTypeEnumProperty == (int)savannah[lineindex,columnindex].CellType)
                    {
                        Collected[lineindex, columnindex] = savannah[i, j];
                    } 
                    ++columnindex;
                }
                ++lineindex;
                columnindex = 0;
            }

            return Collected;
        }

        /// <summary>
        /// Listát készít a szavanna állatairól
        /// </summary>
        /// <returns>AnimalBase típusú lista</returns>
        static List<AnimalBase> GetAllAnimal()
        {
            List<AnimalBase> animals = new List<AnimalBase>();

            foreach (Cell item in savannah)
            {
                foreach (AnimalBase animal in item.GetAnimalList())
                {
                    animals.Add(animal);
                }
            }

            return animals;
        }
    }
}
