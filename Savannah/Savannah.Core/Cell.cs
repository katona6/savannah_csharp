﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    public class Cell : ICloneable
    {
        public Cell()
        {
            Animals = new List<AnimalBase>();
        }

        public Cell(int x, int y)
        {
            coordX = x;
            CoordY = y;
            Animals = new List<AnimalBase>();
        }

        public Cell(FieldType type, Plant plant, List<AnimalBase> animals)
        {
            CellType = type;
            this.Animals = animals;
            if(plant != null) this.plant = plant;
        }

        public Cell(FieldType type, Plant plant, List<AnimalBase> animals, int pXcoord, int pYcoord) : this(type, plant, animals)
        {
            coordX = pXcoord;
            coordY = pYcoord;
        }

        /// <summary>
        /// A cellán elhelyeszkedő növény
        /// </summary>
        private Plant plant;

        /// <summary>
        /// A cella x és y koordinátái
        /// </summary>
        private int coordX, coordY;

        /// <summary>
        /// property a cella x koordinátájához
        /// </summary>
        public int CoordX { get { return coordX; } set { coordX = value; } }


        /// <summary>
        /// property a cella y koordinátájához
        /// </summary>
        public int CoordY { get { return coordY; } set { coordY = value; } }
        /// <summary>
        /// Property a cella növényéhez
        /// </summary>
        public Plant Plantp
        {
            get
            {
                return plant;
            }
        }

        /// <summary>
        /// A cellában tartózkodó állatok listája
        /// </summary>
    //    private List<AnimalBase> animals;

        public List<AnimalBase> Animals
        {
            get;
            set;
        }

        /// <summary>
        /// Property, megadja, hogy a mező milyen típusú, víz vagy szárazföld
        /// </summary>
        public FieldType CellType { get; set; }

        /// <summary>
        /// A mezőre lépő állatot hozzáadja az állatok listájához
        /// </summary>
        /// <param name="p_Animal">Az az állat, amely a mezőre lépne</param>
        /// <returns>Visszaadja, hogy sikerült-e hozzáadni az állatot</returns>
        public bool AddAnimal(AnimalBase p_Animal)
        {
            if ((p_Animal.AnimalTypeEnumProperty == AnimalType.aquatic) && (CellType == FieldType.water))
            {
                if (Animals.Count < 6 && this.GetAdultNumber() < 3)
                {
                    Animals.Add(p_Animal);
                    return true;
                }
                else
                    return false;
            }
            else if ((p_Animal.AnimalTypeEnumProperty == AnimalType.terrestial) && (CellType == FieldType.land))
            {
                if (Animals.Count < 6 && GetAdultNumber() < 3)
                {
                    Animals.Add(p_Animal);
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Az állat eltávolítása a cella listájából
        /// </summary>
        /// <param name="p_Animal">Az az állat amit el akarunk távolítani</param>
        public void RemoveAnimal(AnimalBase p_Animal)
        {
            if (Animals.Count != 0)
            {
                for (int i = 0; i < Animals.Count; ++i)
                {
                    if (Animals[i] == p_Animal)
                    {
                        Animals.RemoveAt(i);
                        break;
                    }
                }
            //    Animals.Remove(p_Animal); 
            }
        }

        /// <summary>
        /// Ha születik egy állat, ezzel lehet hozzáadni a cellához
        /// </summary>
        /// <param name="p_BabyAnimal">Az újszülött állat</param>
        public void AnimalBirth(AnimalBase p_BabyAnimal)
        {
            if (Animals.Count>5)
            {
                Animals.Add(p_BabyAnimal);
            }
        }

        /// <summary>
        /// Visszaadja a cellán tartózkodó állatok listáját
        /// </summary>
        /// <returns>Visszaadja a cellán tartózkodó állatok listáját</returns>
        public List<AnimalBase> GetAnimalList()
        {
            return Animals;
        }

        public int GetAdultNumber()
        {
            int temp = 0;
            foreach (AnimalBase item in Animals)
            {
                if (item.CurrentAge > 4)
                {
                    ++temp;
                }
            }

            return temp;
        }

        internal int GetPredatorCount()
        {
            return (Animals.Where(x => x is Predator)).Count();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
