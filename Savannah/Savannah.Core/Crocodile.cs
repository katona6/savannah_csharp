﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Savannah.Core
{
    /// <summary>
    /// A krokodilok osztálya.
    /// A krokodilokra jellemző tulajdonságokat és viselkedésformákat tartalmazza.
    /// </summary>
    public class Crocodile : Predator
    {
        internal override int RangeOfMove
        {
            get
            {
                return 2;
            }           
        }

        internal override int RangeOfView
        {
            get
            {
                return 4;
            }            
        }


        public Crocodile()
        {
            AnimalTypeEnumProperty = AnimalType.aquatic;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 80 + new Random().Next(-5, 6);
            targetCell = null;
        }

        public Crocodile(int px, int py)
        {
            AnimalTypeEnumProperty = AnimalType.aquatic;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 80 + new Random().Next(-5, 6);
            targetCell = null;
        }

        public Crocodile(int px, int py, Sex ps)
        {
            AnimalTypeEnumProperty = AnimalType.aquatic;
            xCoordinate = px;
            yCoordinate = py;
            HungerLevel = 5;
            ThirstyLevel = 5;
            CurrentAge = 0;
            MaximumAge = 20;
            AttackPower = 80 + new Random().Next(-5, 6);
            SexEnumProperty = ps;
            targetCell = null;
        }


        internal override void Drink(ref Cell thisCell)
        {
            throw new NotImplementedException();
        }

        protected override void Eat()
        {
            throw new NotImplementedException();
        }
    }
}
