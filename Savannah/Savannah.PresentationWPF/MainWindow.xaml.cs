﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Savannah.PresentationWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int minValue = -1;
        int maxValue = int.MaxValue;
        Boolean valid = true;

        SavannahWindow savannahWindow;

        public MainWindow()
        {
            InitializeComponent();
            textBox_waterp.IsEnabled = false;
        }

        //Alkalmazás bezárása Escape billentyű lenyomásával
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        //Bezárás gombbal
        private void button_close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //Szimuláció indítása
        public void button_start_Click(object sender, RoutedEventArgs e)
        {
            if (valid)
            {
                this.Hide();

                Savannah.Core.Savannah.DayPassed += Savannah_DayPassed;
                Savannah.Core.Data data = new Savannah.Core.Data();

                int sizeX;
                int.TryParse(textBox_sizeX.Text, out sizeX);
                data.SavannahSizeX = sizeX;

                int sizeY;
                int.TryParse(textBox_sizeY.Text, out sizeY);
                data.SavannahSizeY = sizeY;

                int crocodileCount;
                int.TryParse(textBox_crocodile.Text, out crocodileCount);
                data.CrocodileNumber = crocodileCount;

                int hyenaCount;
                int.TryParse(textBox_hyena.Text, out hyenaCount);
                data.HyenaNumber = hyenaCount;

                int lionCount;
                int.TryParse(textBox_lion.Text, out lionCount);
                data.LionNumber= lionCount;

                int giraffeCount;
                int.TryParse(textBox_giraffe.Text, out giraffeCount);
                data.GiraffeNumber = giraffeCount;

                int gazelleCount;
                int.TryParse(textBox_gazelle.Text, out gazelleCount);
                data.GazelleNumber = gazelleCount;

                int grassCount;
                int.TryParse(textBox_grass.Text, out grassCount);
                data.PlantNumber = int.Parse(textBox_grass.Text);

                int waterpCount;
                int.TryParse(textBox_waterp.Text, out waterpCount);
                data.PlantNumber += waterpCount;

                savannahWindow = new SavannahWindow(data.SavannahSizeX, data.SavannahSizeY);
                savannahWindow.Show();
                savannahWindow.Activate();
                this.Hide();

                try
                {
                    Thread threadCore = new Thread(() => Core.Savannah.Run(data));

                    threadCore.Start();
                }
                catch(Exception)
                {

                }
            }
        }

        void Savannah_DayPassed(Core.DayPassedEventArgs args)
        {
            Console.WriteLine("Hello");

            savannahWindow.AddDay(args.Args);

        }

        private void textBox_sizeX_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_sizeX.Text, out parsedValue))
            {
                textBox_sizeX.Text = "";
            }

            int x;
            if (int.TryParse(textBox_sizeX.Text, out x))
            {
                if (!(x < maxValue && x > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_sizeX.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_sizeX.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_sizeY_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_sizeY.Text, out parsedValue))
            {
                textBox_sizeY.Text = "";
            }

            int y;
            if (int.TryParse(textBox_sizeY.Text, out y))
            {
                if (!(y < maxValue && y > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_sizeY.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_sizeY.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_grass_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_grass.Text, out parsedValue))
            {
                textBox_grass.Text = "";
            }

            int p;
            if (int.TryParse(textBox_grass.Text, out p))
            {
                if (!(p < maxValue && p > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_grass.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_grass.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_waterp_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_waterp.Text, out parsedValue))
            {
                textBox_waterp.Text = "";
            }

            int p;
            if (int.TryParse(textBox_waterp.Text, out p))
            {
                if (!(p < maxValue && p > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_waterp.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_waterp.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_gazelle_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_gazelle.Text, out parsedValue))
            {
                textBox_gazelle.Text = "";
            }

            int p;
            if (int.TryParse(textBox_gazelle.Text, out p))
            {
                if (!(p < maxValue && p > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_gazelle.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_gazelle.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_giraffe_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_giraffe.Text, out parsedValue))
            {
                textBox_giraffe.Text = "";
            }

            int p;
            if (int.TryParse(textBox_giraffe.Text, out p))
            {
                if (!(p < maxValue && p > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_giraffe.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_giraffe.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_hyena_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_hyena.Text, out parsedValue))
            {
                textBox_hyena.Text = "";
            }

            int p;
            if (int.TryParse(textBox_hyena.Text, out p))
            {
                if (!(p < maxValue && p > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_hyena.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_hyena.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_lion_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_lion.Text, out parsedValue))
            {
                textBox_lion.Text = "";
            }

            int p;
            if (int.TryParse(textBox_lion.Text, out p))
            {
                if (!(p < maxValue && p > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_lion.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_lion.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }

        private void textBox_crocodile_TextChanged(object sender, TextChangedEventArgs e)
        {
            int parsedValue;
            if (!int.TryParse(textBox_crocodile.Text, out parsedValue))
            {
                textBox_crocodile.Text = "";
            }

            int p;
            if (int.TryParse(textBox_crocodile.Text, out p))
            {
                if (!(p < maxValue && p > minValue))
                {
                    label_error.Content = "A beírt értéknek " + minValue + " és " + maxValue + " között kell lennie!";
                    textBox_crocodile.BorderBrush = Brushes.Red;
                    valid = false;
                }
                else {
                    textBox_crocodile.BorderBrush = Brushes.LightSteelBlue;
                    valid = true;
                }
            }
        }
    }
}
