﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Savannah.PresentationWPF
{
    
    static class Images
    {
        internal static Brush dirtpics = new ImageBrush(new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\dirt.png")));

        internal static BitmapImage lionpics= new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\lion.png"));

        internal static BitmapImage gazellepics = new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\gazelle.png"));

        internal static BitmapImage hyenapics = new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\hyena.png"));

        internal static BitmapImage giraffepics = new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\giraffe.png"));

        internal static BitmapImage crocodilepics = new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\crocodile.png"));

        internal static Brush grasspics = new ImageBrush(new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\grass.png")));

        internal static Brush drygrasspics = new ImageBrush(new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\dry_grass.png")));

        internal static BitmapImage emptypics = new BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "\\pictures\\empty.png"));
    }
}
