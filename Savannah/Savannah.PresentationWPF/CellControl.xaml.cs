﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Savannah.Core;

namespace Savannah.PresentationWPF
{
    /// <summary>
    /// Interaction logic for CellControl.xaml
    /// </summary>
    public partial class CellControl : UserControl
    {
        public CellControl()
        {
            InitializeComponent();
        }

        public CellControl(Cell cell)
        {
            lock (cell)
            {
                InitializeComponent();

                if (cell.CellType == FieldType.water)
                {
                    Background = new SolidColorBrush(Colors.Blue);
                }
                else Background = cell.Plantp != null ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Brown);

                if (cell.Animals.Count > 0)
                {
                    List<BitmapImage> adultImages = new List<BitmapImage>();
                    List<BitmapImage> puppyImages = new List<BitmapImage>();

                    BitmapImage image;

                    foreach (AnimalBase currentAnimal in cell.Animals)
                    {
                        image = SelectImage(currentAnimal.GetType());

                        if (currentAnimal.CurrentAge > 4)
                        {
                            adultImages.Add(image);
                        }
                    }

                    ChangeImages(adultImages, puppyImages);


                    this.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Frissíti a control tartalmát
        /// </summary>
        /// <param name="cell">Ez a cella alapján frissíti</param>
        public void Refresh(Cell cell)
        {
            
                List<BitmapImage> adultImages = new List<BitmapImage>();
                List<BitmapImage> puppyImages = new List<BitmapImage>();

                BitmapImage image;

            for (int i = 0; i < cell.Animals.Count; ++i)
            {
                image = SelectImage(cell.Animals[i].GetType());

                if (cell.Animals[i].CurrentAge > 4)
                {
                    adultImages.Add(image);
                }
                else
                {
                    puppyImages.Add(image);
                }
            }

            ChangeImages(adultImages, puppyImages);


            if (cell.Plantp != null)
            {
                if (cell.Plantp.PlantSize > 0)
                {
                    Background = Images.grasspics;
                }
                else
                {
                    Background = Images.drygrasspics;
                }
            }
            else
            {
                Background = Images.dirtpics;
            }
        }

        /// <summary>
        /// megváltoztatja a képeket a controlon
        /// </summary>
        /// <param name="adultImages">a felnőtt állatok képeinek listája</param>
        /// <param name="puppyImages">a kölyökállatok képeinek listája</param>
        protected void ChangeImages(List<BitmapImage> adultImages, List<BitmapImage> puppyImages)
        {
            AnimalA.Source = adultImages.Count > 0 ? adultImages[0] : Images.emptypics;
            AnimalB.Source = adultImages.Count > 1 ? adultImages[1] : Images.emptypics;

            imgPuppyA.Source = puppyImages.Count > 0 ? puppyImages[0] : Images.emptypics;
            imgPuppyB.Source = puppyImages.Count > 1 ? puppyImages[1] : Images.emptypics;
            imgPuppyC.Source = puppyImages.Count > 2 ? puppyImages[2] : Images.emptypics;
        }

        /// <summary>
        /// képet választ az állattípushoz, fontos, hogy a képek meglegyenek
        /// </summary>
        /// <param name="t">az állat típusa</param>
        /// <returns></returns>
        protected BitmapImage SelectImage(Type t)
        {
            if (t==typeof(Lion))
            {
                return Images.lionpics;              
            }
            if (t == typeof(Hyena))
            {
                return Images.hyenapics;
            }
            if (t == typeof(Crocodile))
            {
                return Images.crocodilepics;
            }
            if (t == typeof(Giraffe))
            {
                return Images.giraffepics;
            }
            if (t == typeof(Gazelle))
            {
                return Images.gazellepics;
            }

            throw new Exception("Nincs kép az állatfajhoz");
        }
    }
}
