﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Savannah.Core;


namespace Savannah.PresentationWPF
{
    /// <summary>
    /// Interaction logic for SavannahWindow.xaml
    /// </summary>
    public partial class SavannahWindow : Window
    {
        List<Cell[,]> days;

        CellControl[,] controls;

        int actDay;
        int x;
        int y;

        public SavannahWindow(int xLength, int yLength)
        {
            InitializeComponent();

            days = new List<Cell[,]>();
            controls = new CellControl[xLength, yLength];
            actDay = 0;
            x = xLength;
            y = yLength;

            for (int i = 0; i < y; ++i)
            {                
                layout.RowDefinitions.Add(new RowDefinition());
            }

            foreach (RowDefinition item in layout.RowDefinitions)
            {
                item.Height = new GridLength(1, GridUnitType.Star);
            }


            for (int i = 0; i < x; i++)
            {               
                layout.ColumnDefinitions.Add(new ColumnDefinition());
            }

            foreach (ColumnDefinition item in layout.ColumnDefinitions)
            {
                item.Width = new GridLength(1, GridUnitType.Star);
            }

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    controls[i, j] = new CellControl();
                    Grid.SetColumn(controls[i, j], i);
                    Grid.SetRow(controls[i, j], j);
                    layout.Children.Add(controls[i, j]);
                }
            }
            
        }

        private void btnPrevDay_Click(object sender, RoutedEventArgs e)
        {
            lock (days)
            {
                if (actDay < 1)
                {
                    return;
                }
                else
                {
                    --actDay;

                    for (int i = 0; i < x; ++i)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            controls[i, j].Refresh(days[actDay][i, j]);
                        }
                    }

                    lbCurrentDay.Content = actDay;
                }
            }
        }

        private void btnNextDay_Click(object sender, RoutedEventArgs e)
        {
            lock (days)
            {
                if (actDay < days.Count)
                {
                    for (int i = 0; i < x; ++i)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            controls[i, j].Refresh(days[actDay][i, j]);
                        }
                    }
                    lbCurrentDay.Content = actDay;

                    ++actDay;

                }
                else
                {
                    return;
                }
            }
        }

        /// <summary>
        /// bővíti a napok listáját
        /// </summary>
        /// <param name="cells"></param>
        internal void AddDay(Cell[,] cells)
        {
            days.Add(cells);
        }


        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                Environment.Exit(0);
            }
            catch (Exception)
            {

               
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (days.Count > 0)
            {
                for (int i = 0; i < x; ++i)
                {
                    for (int j = 0; j < y; j++)
                    {
                        controls[i, j].Refresh(days[actDay][i, j]);
                    }
                }
            }
        }
    }
}
