﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Savannah.Core.Test
{
    [TestClass]
    public class SavannahTests
    {
        [TestMethod]
        public void SavannahTestA()
        {
            Data data = new Data();
            data.CrocodileNumber = 0;
            data.GiraffeNumber =5;
            data.LionNumber = 2;
            data.GazelleNumber = 3;
            data.HyenaNumber = 2;
            data.PlantNumber = 10;
            data.SavannahSizeX = 4;
            data.SavannahSizeY = 4;

            Savannah.Init(data);

            int count = 0;

            Cell[,] cells = Savannah.GetAllCells();

            foreach (Cell item in cells)
            {
                count += item.GetAnimalList().Count;
            }

            Assert.AreEqual(12, count);
        }

        [TestMethod]
        public void SavannahInitTestPlants()
        {
            Data data = new Data();

            data.PlantNumber = 1;
            data.GazelleNumber = 1;
            data.SavannahSizeX = 1;
            data.SavannahSizeY = 1;

            Savannah.Init(data);

            Assert.AreEqual(1, Savannah.savannah[0, 0].Animals.Count, "Nem heyezte el az állatot a szavannán az Init()");
            Assert.AreEqual(true, Savannah.savannah[0, 0].Plantp.PlantSize >= 0, "Nem heyezte el a növényt a szavannán az Init()");
        }

        [TestMethod]
        public void SavannahInitTestA()
        {
            Data data = new Data();

            data.SavannahSizeX = 6;
            data.SavannahSizeY = 6;
            data.CrocodileNumber = 0;
            data.GazelleNumber = 5;
            data.GiraffeNumber = 5;
            data.HyenaNumber = 5;
            data.LionNumber = 5;
            data.PlantNumber = 20;

            Savannah.Init(data);

            int countOfPlants = 0;
            int countOfAnimals = 0;

            foreach (Cell item in Savannah.savannah)
            {
                countOfAnimals += item.Animals.Count;
                if (item.Plantp != null)
                {
                    ++countOfPlants;
                }
            }

            Assert.AreEqual(20, countOfAnimals, "nem pakolt fel elég állatot a szavannára az init");
            Assert.AreEqual(20, countOfPlants, "nem pakolt fel elég növényt a szavannára az init");
        }
    }
}
