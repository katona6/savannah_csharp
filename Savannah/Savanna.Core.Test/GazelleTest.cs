﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Savannah.Core.Test
{
    [TestClass]
    public class GazelleTest
    {
        [TestMethod]
        public void MoveTestSuccess()
        {
            Gazelle gazelle = new Gazelle();
            Cell fromCell = new Cell(FieldType.land, null, new List<AnimalBase>() { gazelle });
            
            
            Cell toCell = new Cell(FieldType.land, null, new List<AnimalBase>());
            gazelle.MoveB(ref fromCell, ref toCell);

            Assert.AreEqual(1, toCell.GetAnimalList().Count, "Nem megfelelő számú állat volt a mezőn.");
            Assert.AreEqual(0, fromCell.GetAnimalList().Count);
            Assert.AreEqual(gazelle, toCell.GetAnimalList()[0]);
        }

        [TestMethod]
        public void DrinkTestSuccess()
        {
            Gazelle gazelle = new Gazelle();
            gazelle.ThirstyLevel = 9;
            Cell thisCell = new Cell(FieldType.water, null, new List<AnimalBase>() { gazelle });
            gazelle.Drink(ref thisCell);
            Assert.AreEqual(0, gazelle.ThirstyLevel);
        }

        [TestMethod]
        public void DrinkTestFail()
        {
            Gazelle gazelle = new Gazelle();
            gazelle.ThirstyLevel = 9;
            Lion lion = new Lion();
            Cell thisCell = new Cell(FieldType.water, null, new List<AnimalBase>() { gazelle, lion });
            gazelle.Drink(ref thisCell);
            Assert.AreNotEqual(0, gazelle.ThirstyLevel);
        }
    }
}
