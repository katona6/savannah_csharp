﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Savannah.Core.Test
{
    /// <summary>
    /// Plant osztály metódusainak tulajdonságainak unit tesztjei
    /// </summary>
    [TestClass]
    public class PlantTest
    {
        /// <summary>
        /// A növény méret property tesztje
        /// </summary>
        [TestMethod]
        public void PlantSizePropertyTest()
        {
            Plant plant = new Plant();
            plant.PlantSize = 10;

            Assert.AreEqual(10, plant.PlantSize, "A növény mérete 10");
        }

        /// <summary>
        /// A növény maximális méret property tesztje
        /// </summary>
        [TestMethod]
        public void MaxPlantSizePropertyTest()
        {
            Plant plant = new Plant();
            plant.MaxPlantSize = 10;

            Assert.AreEqual(10, plant.MaxPlantSize, "A növény maximális mérete 10");
        }

        // <summary>
        /// A növény növekedéséért felelős metódus tesztje
        /// </summary>
        [TestMethod]
        public void GrowTest()
        {
            Plant plant = new Plant();
            plant.MaxPlantSize = 10;
            plant.PlantSize = 9;

            plant.Grow();

            Assert.AreEqual(10, plant.PlantSize, "A növény mérete 10 lett");
        }

        // <summary>
        /// Az állat eszik a növényből metódus tesztje
        /// </summary>
        [TestMethod]
        public void TryToEatTest()
        {
            Plant plant = new Plant();
            plant.PlantSize = 5;

            Assert.AreEqual(0, plant.TryToEat(3), "Az állat megevett 3 egységet az 5 egységnyi növényből, éhsége 0");

            plant.PlantSize = 5;
            Assert.AreEqual(1, plant.TryToEat(6), "Az állat megevett 6 egységet az 5 egységnyi növényből, éhsége 1");
        }
    }
}
