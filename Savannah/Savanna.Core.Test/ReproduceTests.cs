﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Savannah.Core.Test
{
    [TestClass]
    public class ReproduceTests
    {
        [TestMethod]
        public void LionReproduceTestMale()
        {
            AnimalBase subject = new Lion(0, 0, Sex.male);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Lion(0,0,Sex.female);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a hímoroszlán");
        }

        [TestMethod]
        public void LionReproduceTestFemale()
        {
            AnimalBase subject = new Lion(0, 0, Sex.female);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Lion(0, 0, Sex.male);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a nőstényoroszlán");
        }

        [TestMethod]
        public void HyenaReproduceTestMale()
        {
            AnimalBase subject = new Hyena(0, 0, Sex.male);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Hyena(0, 0, Sex.female);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a hímhiéna");
        }

        [TestMethod]
        public void HyenaReproduceTestFemale()
        {
            AnimalBase subject = new Hyena(0, 0, Sex.female);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Hyena(0, 0, Sex.male);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a nőstényhiéna");
        }

        [TestMethod]
        public void GazelleReproduceTestMale()
        {
            AnimalBase subject = new Gazelle(0, 0, Sex.male);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Gazelle(0, 0, Sex.female);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a hímgazella");
        }

        [TestMethod]
        public void GazelleReproduceTestFemale()
        {
            AnimalBase subject = new Gazelle(0, 0, Sex.female);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Gazelle(0, 0, Sex.male);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a nősténygazella");
        }

        [TestMethod]
        public void GiraffeReproduceTestMale()
        {
            AnimalBase subject = new Giraffe(0, 0, Sex.male);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Giraffe(0, 0, Sex.female);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a hímzsiráf");
        }

        [TestMethod]
        public void GiraffeReproduceTestFemale()
        {
            AnimalBase subject = new Giraffe(0, 0, Sex.female);
            subject.CurrentAge = 5;
            subject.HungerLevel = 5;
            AnimalBase partner = new Giraffe(0, 0, Sex.male);
            partner.HungerLevel = 5;
            partner.CurrentAge = 5;
            Cell testCell = new Cell(0, 0);
            testCell.Animals = new System.Collections.Generic.List<AnimalBase>() { subject, partner };

            subject.DoTheReproduce(2, partner, ref testCell);

            Assert.AreEqual(4, testCell.Animals.Count, "nem szaporodott a nőstényzsiráf");
        }
    }
}
