﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Savannah.Core.Test
{
    [TestClass]
    public class EatTest
    {
        [TestMethod]
        public void VegetarianGrazeTestA()
        {
            Giraffe giraffe = new Giraffe();
            giraffe.HungerLevel = 3;
            Cell cell = new Cell(FieldType.land, new Plant(9), new List<AnimalBase>() { giraffe });
            giraffe.Graze(ref cell);

            Assert.AreEqual(12, giraffe.HungerLevel, "nem legelt a zsiráf");
        }

        [TestMethod]
        public void VegetarianGrazeTestB()
        {
            Giraffe giraffe = new Giraffe();
            giraffe.HungerLevel = 1;
            Cell cell = new Cell(FieldType.land, new Plant(9), new List<AnimalBase>() { giraffe });
            giraffe.Graze(ref cell);

            Assert.AreEqual(0, cell.Plantp.PlantSize, "nem legelt a zsiráf");
        }

        [TestMethod]
        public void VegetarianGrazeTestC()
        {
            Giraffe giraffe = new Giraffe();
            giraffe.HungerLevel = 10;
            Cell cell = new Cell(FieldType.land, new Plant(7), new List<AnimalBase>() { giraffe });
            giraffe.Graze(ref cell);

            Assert.AreEqual(13, giraffe.HungerLevel, "nem legelt eleget a zsiráf");
        }

        [TestMethod]
        public void VegetarianGrazeTestD()
        {
            Giraffe giraffe = new Giraffe();
            giraffe.HungerLevel = 7;
            Cell cell = new Cell(FieldType.land, new Plant(9), new List<AnimalBase>() { giraffe });
            giraffe.Graze(ref cell);

            Assert.AreEqual(3, cell.Plantp.PlantSize, "A zsiráf a semmit ette");
        }

        [TestMethod]
        public void PredatorHuntTestA()
        {
            Gazelle gazelleweak = new Gazelle(); gazelleweak.SurvivalRate = 85;
            Gazelle gazellestrong = new Gazelle(); gazellestrong.SurvivalRate = 120;
            Lion lion = new Lion(); lion.HungerLevel = 4; lion.AttackPower = 100;
            Cell cell = new Cell(FieldType.land, new Plant(0), new List<AnimalBase>());
            cell.AddAnimal(lion);
            cell.AddAnimal(gazellestrong);
            cell.AddAnimal(gazelleweak);

            lion.Hunt(cell);

            bool actual = (cell.GetAnimalList().Contains(gazelleweak)) ? true : false;

            Assert.AreEqual(false,actual,"az oroszlán rosszul választott prédát, vagy nem lett törölve a celláról a préda");
        }

        [TestMethod]
        public void PredatorHuntTestB()
        {
            Gazelle gazelleweak = new Gazelle(); gazelleweak.SurvivalRate = 85;
            Gazelle gazellestrong = new Gazelle(); gazellestrong.SurvivalRate = 120;
            Lion lion = new Lion(); lion.HungerLevel = 4; lion.AttackPower = 100;
            Cell cell = new Cell(FieldType.land, new Plant(0), new List<AnimalBase>() { lion, gazellestrong, gazelleweak });

            lion.Hunt(cell);

            Assert.AreEqual(14, lion.HungerLevel,"az oroszlán nem evett");
        }

        [TestMethod]
        public void PredatorHuntTestC()
        {
            Gazelle gazelleweak = new Gazelle(); gazelleweak.SurvivalRate = 85;
            Gazelle gazellestrong = new Gazelle(); gazellestrong.SurvivalRate = 120;
            Lion lion = new Lion(); lion.HungerLevel = 4; lion.AttackPower = 100;
            Cell cell = new Cell(FieldType.land, new Plant(0), new List<AnimalBase>() { lion, gazellestrong, gazelleweak });

            lion.Hunt(cell);

            Assert.AreEqual(2,cell.GetAnimalList().Count,"a ragadozó nem választott prédát, vagy nem lett letörölve a celláról a préda");
        }
    }
}
