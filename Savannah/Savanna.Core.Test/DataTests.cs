﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Savannah.Core.Test
{
    [TestClass]
    public class DataTests
    {
        [TestMethod]
        public void DataTestA()
        {
            Data subject = new Data();
            Assert.AreEqual(0, subject.CrocodileNumber, "a konstruktor nem állította be helyesen a krokodilok számát");
            Assert.AreEqual(0, subject.GazelleNumber, "a konstruktor nem állította be helyesen a gazellák számát");
            Assert.AreEqual(0, subject.GiraffeNumber, "a konstruktor nem állította be helyesen a zsiráfok számát");
            Assert.AreEqual(0, subject.HyenaNumber, "a konstruktor nem állította be helyesen a hiénák számát");
            Assert.AreEqual(0, subject.LionNumber, "a konstruktor nem állította be helyesen az oroszlánok számát");
        }

        [TestMethod]
        public void DataTestB()
        {
            Data subject = new Data();
            subject.HyenaNumber = 10;
            subject.GiraffeNumber = 5;
            subject.LionNumber = 20;

            Assert.AreEqual(35, subject.GetCountOfAnimals(), "Nem összegzi a GetCountOfAnimals() fv az állatok számát");
        }

        [TestMethod]
        public void DataTestC()
        {
            Data subject = new Data();
            subject.HyenaNumber = 10;
            subject.GiraffeNumber = 5;
            subject.LionNumber = 20;

            Assert.AreEqual(true, subject.GetMaxOfAnimal() is Lion, "nem a megfelelő fajta állatot választotta ki a GetMaxOfAnimal() fv");
        }
    }
}
