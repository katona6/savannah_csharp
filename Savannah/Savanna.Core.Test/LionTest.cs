﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Savannah.Core.Test
{
    /// <summary>
    /// Oroszlán-tesztelö osztály
    /// </summary>
    [TestClass]
    public class LionTest
    {
        /// <summary>
        /// MOZGáS-metódus-teszt
        /// </summary>
        [TestMethod]
        public void MoveTestSuccess()
        {
            Lion lion = new Lion();
            Cell fromCell = new Cell(FieldType.land, null, new List<AnimalBase>() { lion });


            Cell toCell = new Cell(FieldType.land, null, new List<AnimalBase>());
            lion.MoveB(ref fromCell, ref toCell);

            Assert.AreEqual(1, toCell.GetAnimalList().Count, "Nem megfelelő számú állat volt a mezőn.");
            Assert.AreEqual(0, fromCell.GetAnimalList().Count);
            Assert.AreEqual(lion, toCell.GetAnimalList()[0]);
        }
    }
}
