﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Savannah.Core.Test
{
    [TestClass]
    public class LiveTests
    {
        /// <summary>
        /// Létrehor egy mini életteret az állatoknak, hogy lépjenek, ha nem tudnak enni v szaporodni
        /// </summary>
        /// <returns>3X1 cellás miniszavanna</returns>
        public Cell[,] CreateSmallMap()
        {
            Cell[,] cells = new Cell[3, 1];

            cells[0, 0] = new Cell(FieldType.land, new Plant(2), new List<AnimalBase>(), 0, 0);

            cells[1, 0] = new Cell(FieldType.land, new Plant(2), new List<AnimalBase>(), 1, 0);

            cells[2, 0] = new Cell(FieldType.land, new Plant(8), new List<AnimalBase>(), 2, 0);

            return cells;
        }

        [TestMethod]
        public void VegetarianLiveTestGraze()
        {
            Cell[,] testMap = CreateSmallMap();
            Gazelle subject = new Gazelle(0, 0, Sex.male);
            subject.CurrentAge = 5; subject.HungerLevel = 1;

            testMap[0, 0].AddAnimal(subject);
            testMap[0, 0].Plantp.PlantSize = 5;

            subject.Live(ref testMap);

            Assert.AreEqual(5, subject.HungerLevel, "nem legelt");
            Assert.AreEqual(0, testMap[0, 0].Plantp.PlantSize, "nem fogyott a növényzet");
        }
        
        [TestMethod]
        public void VegetarianLiveTestMove()
        {
            Cell[,] testMap = CreateSmallMap();

            Giraffe subject = new Giraffe(0, 0, Sex.male);
            subject.HungerLevel = 6; subject.CurrentAge = 5;
            testMap[0, 0].AddAnimal(subject);

            subject.Live(ref testMap);

            Assert.AreEqual(0, testMap[0, 0].GetAnimalList().Count, "nem mozgott");
            Assert.AreEqual(1, testMap[2, 0].GetAnimalList().Count, "nem mozgott");
        }

        [TestMethod]
        public void PredatorLiveTestHunt()
        {
            Cell[,] testMap = CreateSmallMap();

            Hyena subject = new Hyena(0, 0, Sex.male);
            subject.HungerLevel = 1; subject.CurrentAge = 5;
            testMap[0, 0].AddAnimal(subject);

            Gazelle foodOfSubject = new Gazelle(0, 0, Sex.female);
            foodOfSubject.SurvivalRate = 40;
            testMap[0, 0].AddAnimal(foodOfSubject);

            subject.Live(ref testMap);

            Assert.AreEqual(10, subject.HungerLevel, "nem étkezett a ragadozó");
            Assert.AreEqual(1, testMap[0, 0].GetAnimalList().Count, "nem lett törölve a celláról az elfogyasztott állat");
        }

        [TestMethod]
        public void PredatorLiveTestMove()
        {
            Cell[,] testMap = CreateSmallMap();

            Lion subject = new Lion(0, 0, Sex.male);
            subject.CurrentAge = 4; subject.HungerLevel = 2;
            testMap[0, 0].AddAnimal(subject);

            Gazelle food = new Gazelle(2, 0, Sex.male);
            food.SurvivalRate = 40;
            testMap[2, 0].AddAnimal(food);

            subject.Live(ref testMap);

            Assert.AreEqual(0, testMap[0, 0].GetAnimalList().Count, "nem mozdult ez a ragadozó");
            Assert.AreEqual(2, testMap[2, 0].GetAnimalList().Count, "nem mozdult ez a ragadozó vagy rossz cellára lépett");
        }
    }
}
